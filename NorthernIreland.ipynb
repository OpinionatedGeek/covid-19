{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Northern Ireland COVID-19 Data\n",
    "\n",
    "Simple line graphs of COVID-19 data for Northern Ireland. Press the ⏩ above to run the code, fetch the data and generate the graphs.\n",
    "\n",
    "There's a notebook for [UK data](UnitedKingdom.ipynb) too, or you can go to the [root](../../tree/work) of the folder."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fetch the data\n",
    "\n",
    "[NI's Department of Health](https://www.health-ni.gov.uk/) ([@healthdpt](https://twitter.com/healthdpt) on Twitter) have put up a [dashboard](https://bit.ly/DoHDashboard) ([direct link](https://app.powerbi.com/view?r=eyJrIjoiZGYxNjYzNmUtOTlmZS00ODAxLWE1YTEtMjA0NjZhMzlmN2JmIiwidCI6IjljOWEzMGRlLWQ4ZDctNGFhNC05NjAwLTRiZTc2MjVmZjZjNSIsImMiOjh9)) of data. I haven't found a nice way of downloading data from PowerBI, but I have found a nasty one.\n",
    "\n",
    "Alternatively Tom E. White maintains a copy of the [data](https://github.com/tomwhite/covid-19-uk-data) for COVID-19 in the UK in a machine-readable format.\n",
    "\n",
    "The EU provides some [data](https://opendata.ecdc.europa.eu/covid19/casedistribution/csv) (no information on tests) for multiple countries in a machine-readable format.\n",
    "\n",
    "The following code will download the Northern Ireland and Ireland data so we can process it.\n",
    "\n",
    "The code fetches the data from the URL for raw data for:\n",
    "* Northern Ireland (querying PowerBI): https://wabi-north-europe-api.analysis.windows.net/public/reports/querydata?synchronous=true\n",
    "* Ireland (extracted from all-world data): https://opendata.ecdc.europa.eu/covid19/casedistribution/csv\n",
    "\n",
    "An [editable, local copy of the data](../edit/data/ni.csv) ([download](files/data/ni.csv)) is available (but it is not regularly updated.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import datetime\n",
    "import json\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy\n",
    "import pandas as pd\n",
    "import requests\n",
    "\n",
    "NI_DATA_URL = \"https://raw.githubusercontent.com/tomwhite/covid-19-uk-data/master/data/covid-19-totals-northern-ireland.csv\"\n",
    "EU_DATA_URL = \"https://opendata.ecdc.europa.eu/covid19/casedistribution/csv\"\n",
    "POWERBI_URL = \"https://wabi-north-europe-api.analysis.windows.net/public/reports/querydata?synchronous=true\"\n",
    "POWERBI_TESTS_QUERY_FILENAME = \"data/bi-tests-query.json\"\n",
    "POWERBI_TOTAL_TESTS_QUERY_FILENAME = \"data/bi-tests-total.json\"\n",
    "POWERBI_DEATHS_QUERY_FILENAME = \"data/bi-deaths-query.json\"\n",
    "POWERBI_DEATHS_NOT_IN_REPORTING_PERIOD_QUERY_FILENAME = \"data/bi-deaths-reported-not-within-reporting-period-query.json\"\n",
    "POWERBI_DEATHS_IN_REPORTING_PERIOD_QUERY_FILENAME = \"data/bi-deaths-reported-within-reporting-period-query.json\"\n",
    "\n",
    "def ordinal_indicator_from_number(value: int) -> str:\n",
    "    ordinals = [\"ᵗʰ\", \"ˢᵗ\", \"ⁿᵈ\", \"ʳᵈ\"]\n",
    "    if value > 100:\n",
    "        return ordinal_indicator_from_number(value % 100)\n",
    "    elif value > 20:\n",
    "        return ordinal_indicator_from_number((value - 20) % 10)\n",
    "    elif value < len(ordinals):\n",
    "        return ordinals[value]\n",
    "    else: return ordinals[0]\n",
    "\n",
    "def fetch_powerbi_data(query_filename):\n",
    "    with open(query_filename, 'r') as query_file:\n",
    "        powerbi_query = query_file.read().rstrip()\n",
    "\n",
    "    headers = {\n",
    "                \"Content-Type\": \"application/json\",\n",
    "                \"X-PowerBI-ResourceKey\": \"df16636e-99fe-4801-a5a1-20466a39f7bf\"\n",
    "              }\n",
    "\n",
    "    response = requests.post(POWERBI_URL, data = powerbi_query, headers = headers, allow_redirects = True)\n",
    "    data = response.json()\n",
    "\n",
    "    dataset = data[\"results\"][0][\"result\"][\"data\"][\"dsr\"][\"DS\"][0][\"PH\"][0]\n",
    "    if \"DM0\" in dataset:\n",
    "        return dataset[\"DM0\"]\n",
    "    return dataset[\"DM1\"]\n",
    "\n",
    "# The PowerBI data keys are all quite opaque to me. I've no idea why some sometimes switch from 'M0' to 'M1'\n",
    "# (or from 'DM0' to 'DM1' above). This is a nasty hack to get around the problem.\n",
    "def dereference_m_value(dataset):\n",
    "    if \"M0\" in dataset[0]:\n",
    "        return dataset[0][\"M0\"]\n",
    "    return dataset[0][\"M1\"]\n",
    "\n",
    "\n",
    "deaths_rows = ((datetime.datetime.fromtimestamp(row[\"C\"][0]/1000), numpy.nan if len(row[\"C\"]) == 1 else row[\"C\"][1]) for row in fetch_powerbi_data(POWERBI_DEATHS_QUERY_FILENAME))\n",
    "deaths_rows = list(deaths_rows)\n",
    "index = pd.date_range(deaths_rows[0][0], deaths_rows[-1][0])\n",
    "index.name = \"Date\"\n",
    "deaths = pd.DataFrame.from_records(deaths_rows, index=[\"Date\"], columns=[\"Date\", \"Deaths\"])\n",
    "\n",
    "# Fix dodgy data\n",
    "deaths = deaths.reindex(index, fill_value=numpy.nan)\n",
    "deaths = deaths.fillna(method='ffill')\n",
    "\n",
    "tests_rows = ((datetime.datetime.fromtimestamp(row[\"C\"][0]/1000), row[\"C\"][1], 0 if len(row[\"C\"]) == 2 else row[\"C\"][2]) for row in fetch_powerbi_data(POWERBI_TESTS_QUERY_FILENAME))\n",
    "tests = pd.DataFrame.from_records(tests_rows, index=[\"Date\"], columns=[\"Date\", \"Tests\", \"ConfirmedCases\"])\n",
    "\n",
    "todays_total_tests, todays_individuals_tested, todays_positive_cases = fetch_powerbi_data(POWERBI_TOTAL_TESTS_QUERY_FILENAME)[0]['C']\n",
    "\n",
    "ni_data = pd.concat([tests, deaths], axis=1)\n",
    "\n",
    "# Save the data so we can look at it if we want.\n",
    "ni_data.to_csv(\"data/ni.csv\")\n",
    "\n",
    "# Drop the latest data if it doesn't include a figure for Deaths.\n",
    "while numpy.isnan(ni_data[-1:][\"Deaths\"][0]):\n",
    "    ni_data.drop(ni_data.tail(1).index,inplace=True)\n",
    "\n",
    "ni_previous_row = ni_data[-2:-1]\n",
    "ni_latest_row = ni_data[-1:]\n",
    "\n",
    "latest_date = ni_latest_row.index[0].date()\n",
    "ordinal_indicator = ordinal_indicator_from_number(latest_date.day)\n",
    "latest_date_text = f\"{latest_date:%-d}{ordinal_indicator} {latest_date:%B %Y}\"\n",
    "\n",
    "# Now let's try to munge the EU data into roughly the same shape as the NI data\n",
    "eu_data = pd.read_csv(EU_DATA_URL, index_col=0, parse_dates=True, date_parser=lambda dt: datetime.datetime.strptime(dt, \"%d/%m/%Y\"))\n",
    "ire_data = eu_data[eu_data[\"countriesAndTerritories\"] == \"Ireland\"]\n",
    "# ire_data[\"Tests\"] = numpy.nan\n",
    "ire_data = ire_data[[\"cases\", \"deaths\"]]\n",
    "ire_data.insert(0, \"Tests\", numpy.nan)\n",
    "ire_data.columns = [\"Tests\", \"DailyConfirmedCases\", \"DailyDeaths\"]\n",
    "ire_data.index = ire_data.index.rename(\"Date\")\n",
    "\n",
    "# Sort so the oldest rows are first\n",
    "ire_data = ire_data.sort_values(\"Date\", ascending=True)\n",
    "ire_data = ire_data.loc[\"2020-03-01\":] # Cut off anything before the start of March\n",
    "ire_data[\"Deaths\"] = ire_data[\"DailyDeaths\"].cumsum()\n",
    "ire_data[\"ConfirmedCases\"] = ire_data[\"DailyConfirmedCases\"].cumsum()\n",
    "ire_data = ire_data[[\"Tests\", \"ConfirmedCases\", \"Deaths\", \"DailyConfirmedCases\", \"DailyDeaths\"]]\n",
    "\n",
    "deaths_not_in_reporting_period = int(\n",
    "    dereference_m_value(\n",
    "        fetch_powerbi_data(POWERBI_DEATHS_NOT_IN_REPORTING_PERIOD_QUERY_FILENAME)\n",
    "    )\n",
    ")\n",
    "deaths_in_reporting_period = int(\n",
    "    dereference_m_value(\n",
    "        fetch_powerbi_data(POWERBI_DEATHS_IN_REPORTING_PERIOD_QUERY_FILENAME)\n",
    "    )\n",
    ")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Daily Tweet\n",
    "\n",
    "Just automate a lot of this so it's consistent every day."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cumulative_deaths = int(ni_latest_row[\"Deaths\"][0])\n",
    "years_to_test_all_NI = \"∞\"\n",
    "remaining_days_to_test_all_NI = \"0\"\n",
    "\n",
    "population_of_NI = 1885400 # Estimate from Wikipedia: https://en.wikipedia.org/wiki/Northern_Ireland\n",
    "days_to_test_all_NI = datetime.timedelta(days=int(population_of_NI / todays_individuals_tested))\n",
    "one_year = datetime.timedelta(days=365)\n",
    "years_to_test_all_NI = int(days_to_test_all_NI / one_year)\n",
    "remaining_days_to_test_all_NI = (days_to_test_all_NI - (years_to_test_all_NI * one_year)).days\n",
    "\n",
    "deaths_since_last_report = deaths_not_in_reporting_period + deaths_in_reporting_period\n",
    "print(f\"\"\"There have been {cumulative_deaths} deaths in total ({deaths_in_reporting_period} today, {deaths_since_last_report} since last report) associated with #covid19 in #northernireland as of {latest_date_text}.\n",
    "\n",
    "Test results for {todays_individuals_tested} people were entered. At this rate it would take {years_to_test_all_NI} years and {remaining_days_to_test_all_NI} days to test the entirety of NI.\"\"\")\n",
    "\n",
    "chart = ni_data.drop(\"ConfirmedCases\", 1).drop(\"Tests\", 1).fillna(0).plot(title=f\"Cumulative Deaths (NI) - {latest_date_text}\", color=[\"red\"])\n",
    "label = chart.set_xlabel(f\"Recorded date of death (NOT date of reporting)\")\n",
    "label = chart.set_ylabel(\"Cumulative Deaths\")\n",
    "\n",
    "ni_dpd = ni_data['Deaths']-ni_data['Deaths'].shift(1).fillna(0)\n",
    "ni_dpd = ni_dpd.loc[\"2020-03-17\":]\n",
    "\n",
    "_, ax = plt.subplots()\n",
    "\n",
    "rolling_ni_deaths_per_day = pd.DataFrame(ni_dpd.rolling(window=7).mean()).rename(columns={\"Deaths\": \"7-Day Average\"})\n",
    "ax_7day = rolling_ni_deaths_per_day.plot(use_index=False, legend=True, color=[\"darkred\"], ax=ax)\n",
    "\n",
    "# rolling_ni_deaths_per_day = pd.DataFrame(ni_dpd.rolling(window=3).mean()).rename(columns={\"Deaths\": \"3-Day Average\"})\n",
    "# ax_3day = rolling_ni_deaths_per_day.plot(use_index=False, legend=True, color=[\"indianred\"], ax=ax)\n",
    "\n",
    "ax_bars = pd.DataFrame(ni_dpd).plot(title=f\"Deaths Per Day (NI) - {latest_date_text}\", legend=True, color=[\"red\"], kind=\"bar\", ax=ax)\n",
    "label = ax_bars.set_xlabel(\"Recorded Date Of Death\")\n",
    "label = ax_bars.set_ylabel(\"Deaths\")\n",
    "\n",
    "def label_formatter(value, tick_number):\n",
    "    if (len(ni_dpd.index) - tick_number - 1) % 7 == 0:\n",
    "        return ni_dpd.index[value].strftime(\"%Y-%m-%d\")\n",
    "    else:\n",
    "        return None\n",
    "\n",
    "ax.set_xticklabels(ax.get_xticklabels(), rotation=30)\n",
    "ax.xaxis.set_major_formatter(plt.FuncFormatter(label_formatter))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tests"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "chart = ni_data.drop(\"ConfirmedCases\", 1).drop(\"Deaths\", 1).plot(title=f\"Cumulative Tests (NI) - {latest_date_text}\", color=[\"green\"])\n",
    "label = chart.set_ylabel(\"Cumulative Tests\")\n",
    "chart = ni_data.drop(\"ConfirmedCases\", 1).drop(\"Deaths\", 1).plot(title=f\"Cumulative Tests Log Scale (NI) - {latest_date_text}\", color=[\"green\"], logy=True)\n",
    "label = chart.set_ylabel(\"Cumulative Tests (Log Scale)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Confirmed Infections"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "chart = ni_data.drop(\"Tests\", 1).drop(\"Deaths\", 1).plot(title=f\"Cumulative Confirmed Infections (NI) - {latest_date_text}\", color=[\"blue\"])\n",
    "label = chart.set_ylabel(\"Cumulative Confirmed Infections\")\n",
    "chart = ni_data.drop(\"Tests\", 1).drop(\"Deaths\", 1).plot(title=f\"Cumulative Confirmed Infections Log Scale (NI) - {latest_date_text}\", color=[\"blue\"], logy=True)\n",
    "label = chart.set_ylabel(\"Cumulative Confirmed Infections (Log Scale)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Deaths"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "chart = ni_data.drop(\"ConfirmedCases\", 1).drop(\"Tests\", 1).plot(title=f\"Cumulative Deaths (NI) - {latest_date_text}\", color=[\"red\"])\n",
    "label = chart.set_ylabel(\"Cumulative Deaths\")\n",
    "chart = ni_data.drop(\"ConfirmedCases\", 1).drop(\"Tests\", 1).plot(title=f\"Cumulative Deaths Log Scale (NI) - {latest_date_text}\", color=[\"red\"], logy=True)\n",
    "label = chart.set_ylabel(\"Cumulative Deaths (Log Scale)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Deaths Per Day"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ni_deaths_per_day = ni_data['Deaths']-ni_data['Deaths'].shift(1).fillna(0)\n",
    "ni_deaths_per_day = ni_deaths_per_day.loc[\"2020-03-17\":] # Cut off anything before the St. Patrick's Day - there were no COVID-19 deaths before then\n",
    "\n",
    "chart = ni_deaths_per_day.plot(title=f\"Deaths Per Day (NI) - {latest_date_text}\", color=[\"red\"], kind=\"bar\")\n",
    "label = chart.set_xticklabels(map(lambda x: x.strftime(\"%Y-%m-%d\"), ni_deaths_per_day.index))\n",
    "label = chart.set_ylabel(\"Deaths\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Deaths Per Day (Rolling 3-Day Average)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rolling_ni_deaths_per_day = ni_deaths_per_day.rolling(window=3).mean()\n",
    "chart = rolling_ni_deaths_per_day.plot(title=f\"3-Day Average Deaths Per Day (NI) - {latest_date_text}\", color=[\"red\"], kind=\"bar\")\n",
    "label = chart.set_xticklabels(map(lambda x: x.strftime(\"%Y-%m-%d\"), rolling_ni_deaths_per_day.index))\n",
    "label = chart.set_ylabel(\"Deaths (3-Day Mean)\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Deaths Per Day (Rolling 7-Day Average)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rolling_ni_deaths_per_day = ni_deaths_per_day.rolling(window=7).mean()\n",
    "chart = rolling_ni_deaths_per_day.plot(title=f\"7-Day Average Deaths Per Day (NI) - {latest_date_text}\", color=[\"red\"], kind=\"bar\")\n",
    "label = chart.set_xticklabels(map(lambda x: x.strftime(\"%Y-%m-%d\"), rolling_ni_deaths_per_day.index))\n",
    "label = chart.set_ylabel(\"Deaths (7-Day Mean)\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## All NI Data (Combined)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "chart = ni_data.plot(title=f\"All Data (NI) - {latest_date_text}\", color=[\"green\", \"blue\", \"red\"])\n",
    "chart = ni_data.plot(title=f\"All Data (NI) (Log Scale) - {latest_date_text}\", color=[\"green\", \"blue\", \"red\"], logy=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## All-Ireland"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ni_deaths = ni_data[\"Deaths\"].rename(\"Northern Ireland\")\n",
    "ire_deaths = ire_data[\"Deaths\"].rename(\"Republic of Ireland\")\n",
    "\n",
    "data = pd.DataFrame([ni_deaths, ire_deaths]).transpose()\n",
    "data = data.loc[\"2020-03-01\":] # Cut off anything before the start of March\n",
    "\n",
    "chart = data.plot(title=f\"Cumulative Deaths (All Ireland) - {latest_date_text}\", color=[\"blue\", \"green\"])\n",
    "label = chart.set_ylabel(\"Cumulative Deaths\")\n",
    "\n",
    "chart = data.plot(title=f\"Cumulative Deaths Log Scale (All Ireland) - {latest_date_text}\", color=[\"blue\", \"green\"], logy=True)\n",
    "label = chart.set_ylabel(\"Cumulative Deaths (Log Scale)\")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "NORTHERN_IRELAND_POPULATION = 1885400 # Estimate from Wikipedia: https://en.wikipedia.org/wiki/Northern_Ireland\n",
    "IRELAND_POPULATION = 4921500 # Estimate from Wikipedia: https://en.wikipedia.org/wiki/Republic_of_Ireland\n",
    "\n",
    "ni_hundred_ks = NORTHERN_IRELAND_POPULATION / 100000\n",
    "ire_hundred_ks = IRELAND_POPULATION / 100000\n",
    "ni_millions = NORTHERN_IRELAND_POPULATION / 1000000\n",
    "ire_millions = IRELAND_POPULATION / 1000000\n",
    "\n",
    "print(f\"NI millions: {ni_millions}\")\n",
    "print(f\"IRE millions: {ire_millions}\")\n",
    "\n",
    "per110k_data = data.copy()\n",
    "per110k_data[\"Northern Ireland\"] = per110k_data[\"Northern Ireland\"] / ni_hundred_ks\n",
    "per110k_data[\"Republic of Ireland\"] = per110k_data[\"Republic of Ireland\"] / ire_hundred_ks\n",
    "\n",
    "chart = per110k_data.plot(title=f\"COVID-19 Deaths Per 100k Population (All Ireland) - {latest_date_text}\", color=[\"blue\", \"green\"])\n",
    "label = chart.set_ylabel(\"Cumulative Deaths\")\n",
    "chart = per110k_data.plot(title=f\"COVID-19 Deaths Per 100k Population Log Scale (All Ireland) - {latest_date_text}\", color=[\"blue\", \"green\"], logy=True)\n",
    "label = chart.set_ylabel(\"Cumulative Deaths (Log Scale)\")\n",
    "\n",
    "\n",
    "permillion_data = data.copy()\n",
    "permillion_data[\"Northern Ireland\"] = permillion_data[\"Northern Ireland\"] / ni_millions\n",
    "permillion_data[\"Republic of Ireland\"] = permillion_data[\"Republic of Ireland\"] / ire_millions\n",
    "\n",
    "chart = permillion_data.plot(title=f\"COVID-19 Deaths Per Million Population (All Ireland) - {latest_date_text}\", color=[\"blue\", \"green\"])\n",
    "label = chart.set_ylabel(\"Cumulative Deaths\")\n",
    "chart = permillion_data.plot(title=f\"COVID-19 Deaths Per Million Population Log Scale (All Ireland) - {latest_date_text}\", color=[\"blue\", \"green\"], logy=True)\n",
    "label = chart.set_ylabel(\"Cumulative Deaths (Log Scale)\")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "THRESHOLD = 0.25\n",
    "\n",
    "trimmer = per110k_data[\"Northern Ireland\"] > THRESHOLD\n",
    "ni_per_100ks = per110k_data.loc[trimmer][\"Northern Ireland\"].reset_index().drop(\"Date\", 1)\n",
    "\n",
    "trimmer = per110k_data[\"Republic of Ireland\"] > THRESHOLD\n",
    "ire_per_100ks = per110k_data.loc[trimmer][\"Republic of Ireland\"].reset_index().drop(\"Date\", 1)\n",
    "\n",
    "per_100k_from_threshold = pd.DataFrame(ni_per_100ks).join(pd.DataFrame(ire_per_100ks))\n",
    "\n",
    "plot = per_100k_from_threshold.plot(title=f\"Cumulative COVID-19 Deaths Per 100k Population\", color=[\"blue\", \"green\"])\n",
    "label = plot.set_ylabel(\"Deaths per 100k of population\")\n",
    "label = plot.set_xlabel(f\"Days since crossing {THRESHOLD} deaths per 100k threshold\")\n",
    "\n",
    "plot = per_100k_from_threshold.plot(title=f\"Cumulative COVID-19 Deaths Per 100k Population Log Scale \", color=[\"blue\", \"green\"], logy=True)\n",
    "label = plot.set_ylabel(\"Deaths per 100k of population (Log Scale)\")\n",
    "label = plot.set_xlabel(f\"Days since crossing {THRESHOLD} deaths per 100k threshold\")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ni_tests_per_day = ni_data['Tests']-ni_data['Tests'].shift(1).fillna(0)\n",
    "ni_tests_per_day = ni_tests_per_day.loc[\"2020-03-17\":] # Cut off anything before St. Patrick's Day\n",
    "if numpy.isnan(ni_tests_per_day[-1]):\n",
    "    ni_tests_per_day[-1] = 0\n",
    "\n",
    "rolling_ni_tests_per_day = ni_tests_per_day.rolling(window=7).sum()\n",
    "chart = rolling_ni_tests_per_day.plot(title=f\"Tests Per Week (NI) - {latest_date_text}\", color=[\"blue\"], kind=\"bar\")\n",
    "label = chart.set_xticklabels(map(lambda x: x.strftime(\"%Y-%m-%d\"), rolling_ni_tests_per_day.index))\n",
    "label = chart.set_ylabel(\"Rolling Tests Per Week\")\n",
    "\n",
    "ni_tests_last_week = rolling_ni_tests_per_day[-1]\n",
    "ni_tests_last_week_as_percentage_of_population = (ni_tests_last_week / NORTHERN_IRELAND_POPULATION) * 100\n",
    "ni_tests_last_week_as_percentage_of_population\n",
    "\n",
    "print(f\"\"\"Ireland aims to test 2% of its population every week. Or does it? I can't find any source for this besides this tweet. https://twitter.com/laineydoyle/status/1249155188394426369\n",
    "\n",
    "This week #northernireland tested {ni_tests_last_week / NORTHERN_IRELAND_POPULATION:.3%} of its population.\"\"\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ni_cases_per_day = ni_data['ConfirmedCases']-ni_data['ConfirmedCases'].shift(1).fillna(0)\n",
    "ni_cases_per_day = ni_cases_per_day.loc[\"2020-03-17\":] # Cut off anything before the St. Patrick's Day - there were no COVID-19 deaths before then\n",
    "\n",
    "chart = ni_cases_per_day.plot(title=f\"Confirmed Cases Per Day (NI) - {latest_date_text}\", color=[\"blue\"], kind=\"bar\")\n",
    "label = chart.set_xticklabels(map(lambda x: x.strftime(\"%Y-%m-%d\"), ni_cases_per_day.index))\n",
    "label = chart.set_ylabel(\"Confirmed Cases\")\n",
    "\n",
    "chart.set_xticklabels(ax.get_xticklabels())\n",
    "chart.xaxis.set_major_formatter(plt.FuncFormatter(label_formatter))\n",
    "\n",
    "rolling_ni_confirmedcases_per_day = pd.DataFrame(ni_cases_per_day.rolling(window=7).mean()).rename(columns={\"ConfirmedCases\": \"7-Day Average\"})\n",
    "chart = rolling_ni_confirmedcases_per_day.plot(use_index=False, legend=True, color=[\"darkblue\"], ax=chart)\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
